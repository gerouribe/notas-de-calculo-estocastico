\contentsline {chapter}{\tocchapter {Cap\'itulo}{1}{Preliminares}}{1}{chapter.1}
\contentsline {section}{\tocsection {}{1}{Funciones de variaci\'on acotada y la integral de Lebesgue-Stieltjes}}{1}{section.1.1}
\contentsline {section}{\tocsection {}{2}{Espacios de Hilbert y proyecciones ortogonales}}{7}{section.1.2}
\contentsline {section}{\tocsection {}{3}{Preliminares de procesos estoc\'asticos}}{10}{section.1.3}
\contentsline {section}{\tocsection {}{4}{Martingalas a tiempo continuo}}{15}{section.1.4}
\contentsline {chapter}{\tocchapter {Cap\'itulo}{2}{Integraci\'on estoc\'astica respecto del movimiento browniano}}{20}{chapter.2}
\contentsline {section}{\tocsection {}{1}{El movimiento browniano y su variaci\'on cuadr\'atica}}{21}{section.2.1}
\contentsline {section}{\tocsection {}{2}{Ejemplo de una filtraci\'on que satisface las condiciones habituales}}{24}{section.2.2}
\contentsline {section}{\tocsection {}{3}{La integral estoc\'astica respecto del movimiento browniano}}{25}{section.2.3}
\contentsline {section}{\tocsection {}{4}{Ecuaciones diferenciales estoc\'asticas conducidas por el movimiento browniano}}{34}{section.2.4}
\contentsline {chapter}{\tocchapter {Cap\'itulo}{3}{Integraci\'on estoc\'astica respecto de semi-martingalas continuas}}{45}{chapter.3}
\contentsline {section}{\tocsection {}{1}{Martingalas continuas y su variaci\'on cuadr\'atica}}{45}{section.3.1}
\contentsline {section}{\tocsection {}{2}{La integral estoc\'astica respecto de martingalas continuas cuadrado integrables}}{54}{section.3.2}
\contentsline {subsection}{\tocsubsection {}{2.1}{Construcci\'on directa de la integral estoc\'astica}}{57}{subsection.3.2.1}
\contentsline {subsection}{\tocsubsection {}{2.2}{Construcci\'on de la integral estoc\'astica mediante el teorema de representaci\'on de Riesz}}{62}{subsection.3.2.2}
\contentsline {section}{\tocsection {}{3}{La integral estoc\'astica respecto de semimartingalas}}{62}{section.3.3}
\contentsline {section}{\tocsection {}{4}{La f\'ormula de It\^o}}{65}{section.3.4}
\contentsline {chapter}{\tocchapter {Cap\'itulo}{4}{Aplicaciones a la integral estoc\'astica}}{68}{chapter.4}
\contentsline {section}{\tocsection {}{1}{La exponencial estoc\'astica}}{68}{section.4.1}
\contentsline {section}{\tocsection {}{2}{El teorema de caracterizaci\'on de L\'evy}}{69}{section.4.2}
\contentsline {section}{\tocsection {}{3}{Martingalas locales continuas como cambios de tiempo del movimiento browniano}}{71}{section.4.3}
\contentsline {section}{\tocsection {}{4}{La norma del movimiento browniano en $\ensuremath {\mathbb {R}}^d$}}{75}{section.4.4}
\contentsline {section}{\tocsection {}{5}{El movimiento browniano complejo}}{82}{section.4.5}
\contentsline {section}{\tocsection {}{6}{Movimiento browniano y funciones arm\'onicas}}{86}{section.4.6}
\contentsline {section}{\tocsection {}{7}{La f\'ormula de Feynman-Kac}}{89}{section.4.7}
\contentsline {section}{\tocsection {}{8}{El teorema de Girsanov}}{96}{section.4.8}
\contentsline {chapter}{\tocchapter {Cap\'itulo}{5}{Integral estoc\'astica respecto de semimartingalas}}{102}{chapter.5}
\contentsline {section}{\tocsection {}{1}{Integrales de Lebesgue-Stieltjes respecto de procesos de Poisson}}{102}{section.5.1}
\contentsline {section}{\tocsection {}{2}{Medidas de Poisson aleatorias}}{103}{section.5.2}
\contentsline {section}{\tocsection {}{3}{Procesos de L\'evy y la descomposici\'on de L\'evy-It\^o}}{106}{section.5.3}
\contentsline {section}{\tocsection {}{4}{La descomposici\'on de Doob-Meyer}}{106}{section.5.4}
\contentsline {section}{\tocsection {}{5}{La integral estoc\'astica y sus propiedades}}{111}{section.5.5}
\contentsline {chapter}{\tocchapter {Cap\'itulo}{}{Bibliograf\'{\i }a}}{112}{chapter*.3}
